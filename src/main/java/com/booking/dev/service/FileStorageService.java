package com.booking.dev.service;

import io.vavr.control.Try;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {

    Try<String> storageFile(MultipartFile file, String nameFood);

    Boolean generateFolderFoodImage(String nameFood);

}