package com.booking.dev.service;

import com.booking.dev.domain.Item;
import com.booking.dev.domain.Order;
import com.booking.dev.webrest.dto.OrderCreateDTO;
import io.vavr.control.Try;

import java.util.List;

public interface OrderService {

    Try<Boolean> create(OrderCreateDTO dto);

    List<Order> fetchAll();

    List<Order> fetchByUserId(String userId);

    List<Item> fetchDetail(String _id);

}
