package com.booking.dev.service.data;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileUploadInfo {

    private String fileDes;
    private String pathResources;
    private Long size;

}