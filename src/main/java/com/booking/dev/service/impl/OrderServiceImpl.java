package com.booking.dev.service.impl;

import com.booking.dev.domain.Food;
import com.booking.dev.domain.Item;
import com.booking.dev.domain.Order;
import com.booking.dev.repository.FoodRepository;
import com.booking.dev.repository.ItemRepository;
import com.booking.dev.repository.OrderRepository;
import com.booking.dev.service.OrderService;
import com.booking.dev.webrest.dto.OrderCreateDTO;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);
    private final OrderRepository orderRepository;
    private final ItemRepository itemRepository;
    private final FoodRepository foodRepository;
    public OrderServiceImpl(OrderRepository orderRepository,
                            ItemRepository itemRepository,
                            FoodRepository foodRepository){
        this.orderRepository = orderRepository;
        this.itemRepository = itemRepository;
        this.foodRepository = foodRepository;
    }
    @Override
    public Try<Boolean> create(OrderCreateDTO dto) {

        // TODO : Save Order before
        Order orderSaved = orderRepository.save(new Order(dto.getUserId(), Instant.now().toString()));

        for (int i = 0 ; i < dto.getItemFoods().size() ; i++){
            Food food = foodRepository.findById(dto.getItemFoods().get(i).getIdFood()).get();
            Item item = new Item(orderSaved.get_id(), food, dto.getItemFoods().get(i).getQuantity());
            itemRepository.save(item);
        }
        return Try.of(() -> true);
    }

    @Override
    public List<Order> fetchAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> fetchByUserId(String userId) {
        return orderRepository.findAllByUserId(userId);
    }

    @Override
    public List<Item> fetchDetail(String _id) {
        return itemRepository.findAllByOrderId(_id);
    }

}
