package com.booking.dev.service.impl;

import com.booking.dev.service.FileStorageService;
import com.booking.dev.service.FileUploadService;
import com.booking.dev.service.data.FileUploadInfo;
import io.vavr.control.Try;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class FileUploadServiceImpl implements FileUploadService {


    private final FileStorageService fileStorageService;

    public FileUploadServiceImpl(FileStorageService fileStorageService){
        this.fileStorageService = fileStorageService;
    }


    @Override
    public Try<FileUploadInfo> upLoadSingleFile(MultipartFile file, String nameFood) {

        Try<String> resultStorageFile = fileStorageService.storageFile(file,nameFood);

        if (resultStorageFile.isFailure()){
            return Try.failure(new Exception(resultStorageFile.getCause().getMessage()));
        }
        return Try.of(() -> {
            return new FileUploadInfo(file.getOriginalFilename(), resultStorageFile.get(), file.getSize());
        });
    }


}
