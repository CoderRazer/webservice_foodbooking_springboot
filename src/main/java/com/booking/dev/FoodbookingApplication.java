package com.booking.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodbookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodbookingApplication.class, args);
	}

}
