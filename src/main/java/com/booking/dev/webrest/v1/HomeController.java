package com.booking.dev.webrest.v1;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/access")
@CrossOrigin(origins = "http://localhost:4000" , maxAge = 3600)
public class HomeController {

    @GetMapping("/all")
    public String allAccess(){
        return "Public Content FoodBooking From Spring Boot Server";
    }

}
