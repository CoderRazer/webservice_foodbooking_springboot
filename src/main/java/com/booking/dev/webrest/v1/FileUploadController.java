package com.booking.dev.webrest.v1;

import com.booking.dev.service.FileStorageService;
import com.booking.dev.service.FileUploadService;
import com.booking.dev.service.data.FileUploadInfo;
import com.booking.dev.webrest.response.EmptyDataObject;
import com.booking.dev.webrest.response.GenericBaseResponse;
import com.booking.dev.webrest.response.UploadFileResponse;
import io.vavr.control.Try;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URISyntaxException;

@CrossOrigin(origins = "http://localhost:4000")
@RestController
@RequestMapping("/api/access")
public class FileUploadController {

    private final FileUploadService fileUploadService;
    private final FileStorageService fileStorageService;

    public FileUploadController(FileUploadService fileUploadService,
                                FileStorageService fileStorageService){
        this.fileUploadService = fileUploadService;
        this.fileStorageService = fileStorageService;
    }

    @PostMapping("/food/uploadFile")
    public ResponseEntity<UploadFileResponse> uploadFile(@RequestParam("file")MultipartFile file, @RequestParam("nameFood") String nameFood)throws URISyntaxException {

        if (fileStorageService.generateFolderFoodImage(nameFood)){
            Try<FileUploadInfo> result = fileUploadService.upLoadSingleFile(file,nameFood);

            UploadFileResponse response = new UploadFileResponse();
            GenericBaseResponse.Meta meta = new GenericBaseResponse.Meta();

            if (result.isSuccess()){
                val successResult = (Try.Success) result;
                meta.setErrorDetail("No Error");
                meta.setStatusCode(200);
                meta.setMessage("Upload Successfully!");
                response.setData(successResult.get());
            }
            if (result.isFailure()){
                val failureResult = (Try.Failure) result;
                meta.setErrorDetail(failureResult.getCause().getMessage());
                meta.setStatusCode(500);
                meta.setMessage("Upload Failure!");
                response.setData(new EmptyDataObject());
            }
            response.setMeta(meta);
            return new ResponseEntity<UploadFileResponse>(response, HttpStatus.OK);
        }
        return new ResponseEntity<UploadFileResponse>(new UploadFileResponse(), HttpStatus.OK);
    }



}
