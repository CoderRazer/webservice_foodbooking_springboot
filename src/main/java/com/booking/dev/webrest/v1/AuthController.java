package com.booking.dev.webrest.v1;

import com.booking.dev.authentication.UserPrinciple;
import com.booking.dev.domain.User;
import com.booking.dev.jwt.JwtProvider;
import com.booking.dev.repository.UserRepository;
import com.booking.dev.webrest.request.LoginRequest;
import com.booking.dev.webrest.request.RegisterRequest;
import com.booking.dev.webrest.response.RestResponseUtils;
import com.booking.dev.webrest.response.VueResponse;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*" , maxAge = 3600)
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/login")
    public ResponseEntity <?> login(@Valid @RequestBody LoginRequest loginRequest) throws URISyntaxException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateJwtToken(authentication);

        UserPrinciple userPrinciple = (UserPrinciple) authentication.getPrincipal();

        if (userPrinciple.getAdmin().equals(false)){
            return new ResponseEntity<String>("Sorry! You must is admin to access this resources", HttpStatus.FORBIDDEN);
        }

        return ResponseEntity.ok(new VueResponse(jwt,userPrinciple.get_id(), userPrinciple.getEmail(), userPrinciple.getAdmin()));
    }

    @PostMapping("/login-mobile")
    public ResponseEntity <?> loginMobile(@Valid @RequestBody LoginRequest loginRequest) throws URISyntaxException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserPrinciple userPrinciple = (UserPrinciple) authentication.getPrincipal();

        return new ResponseEntity<>(RestResponseUtils.create(
                Option.of(userRepository.findByEmail(userPrinciple.getEmail())).toTry(),"GET ONE CATEGORY",200),HttpStatus.OK);


    }

    @PostMapping("/register")
    public ResponseEntity <?> register(@Valid @RequestBody RegisterRequest registerRequest) throws URISyntaxException {

        if (userRepository.existsUserByEmail(registerRequest.getEmail())){
            return new ResponseEntity<String>("Failure when register -> Email is already in use", HttpStatus.BAD_REQUEST);
        }

        // TODO : Create new user web admin
        User user = new User(
                registerRequest.getName(),
                registerRequest.getEmail(),
                passwordEncoder.encode(registerRequest.getPassword()),
                registerRequest.getPhone(),
                registerRequest.getAddress(),
                true);

        Try result = Option.of(userRepository.save(user)).toTry();

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"REGISTER SUCCESS",200),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"REGISTER FAILURE",500),HttpStatus.OK);
        }
    }

    @PostMapping("/register-mobile")
    public ResponseEntity <?> registerMobile(@Valid @RequestBody RegisterRequest registerRequest) throws URISyntaxException {

        if (userRepository.existsUserByEmail(registerRequest.getEmail())){
            return new ResponseEntity<String>("Failure when register -> Email is already in use", HttpStatus.BAD_REQUEST);
        }

        // TODO : Create new user mobile
        User user = new User(
                registerRequest.getName(),
                registerRequest.getEmail(),
                passwordEncoder.encode(registerRequest.getPassword()),
                registerRequest.getPhone(),
                registerRequest.getAddress(),
                false);

        Try result = Option.of(userRepository.save(user)).toTry();

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"REGISTER SUCCESS",200),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"REGISTER FAILURE",500),HttpStatus.OK);
        }
    }

}
