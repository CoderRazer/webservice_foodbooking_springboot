package com.booking.dev.webrest.v1;

import com.booking.dev.domain.Category;
import com.booking.dev.repository.CategoryRepository;
import com.booking.dev.repository.custom.CategoryRepositoryCustom;
import com.booking.dev.webrest.dto.CategoryCreateDTO;
import com.booking.dev.webrest.response.RestResponseUtils;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/access")
public class CategoryController {

    private final CategoryRepository categoryRepository;
    private final CategoryRepositoryCustom categoryRepositoryCustom;

    public CategoryController(CategoryRepository categoryRepository,
                              CategoryRepositoryCustom categoryRepositoryCustom){
        this.categoryRepository = categoryRepository;
        this.categoryRepositoryCustom = categoryRepositoryCustom;
    }

    private final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @GetMapping("/categorys")
    public ResponseEntity<?> findAll() throws URISyntaxException {

        return new ResponseEntity<>(RestResponseUtils.create(
                Option.of(categoryRepository.findAll()).toTry(),"GET LIST CATEGORY",200),HttpStatus.OK);
    }

    @GetMapping("/category/{_id}")
    public ResponseEntity<?> findOne(@PathVariable String _id) throws URISyntaxException {
        return new ResponseEntity<>(RestResponseUtils.create(
                Option.of(categoryRepository.findById(_id)).toTry(),"GET ONE CATEGORY",200),HttpStatus.OK);
    }

    @GetMapping("/category:searchByName")
    public ResponseEntity<?> searchByName(@Param("name") String name) throws URISyntaxException {
        return new ResponseEntity<>(RestResponseUtils.create(
                Option.of(categoryRepository.findAllByNameContainsOrderByName(name)).toTry(),"FETCH CATEGORY BY NAME",200),HttpStatus.OK);
    }

    @PostMapping("/category:create")
    public ResponseEntity<?> createCategory(@Valid @RequestBody CategoryCreateDTO dto) throws URISyntaxException {

        Try result = Option.of(categoryRepository.save(new Category(dto.getName(),dto.getPathImage()))).toTry();

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"CREATE CATEGORY SUCCESS",200),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"CREATE CATEGORY FAILURE",500),HttpStatus.OK);
        }
    }

    @PutMapping("/category:update/{_id}")
    public ResponseEntity<?> update(@PathVariable String _id, @Valid @RequestBody CategoryCreateDTO dto) throws URISyntaxException {

        Try<Boolean> result = categoryRepositoryCustom.update(_id,dto.getName());

        if (result.isSuccess()){
            return new ResponseEntity<>(RestResponseUtils.create(result,"UPDATE CATEGORY SUCCESS",200),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(RestResponseUtils.create(result,"UPDATE CATEGORY FAILURE",500),HttpStatus.OK);
        }
    }

}
