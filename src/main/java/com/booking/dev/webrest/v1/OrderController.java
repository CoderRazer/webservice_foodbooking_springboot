package com.booking.dev.webrest.v1;

import com.booking.dev.service.OrderService;
import com.booking.dev.webrest.dto.OrderCreateDTO;
import com.booking.dev.webrest.response.RestResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/access")
@CrossOrigin(origins = "*" , maxAge = 3600)
public class OrderController {

    private final Logger logger = LoggerFactory.getLogger(OrderController.class);
    private final OrderService orderService;

    public OrderController(OrderService orderService){
        this.orderService = orderService;
    }

    @PostMapping("/order:create")
    public ResponseEntity<?> create(@Valid @RequestBody OrderCreateDTO dto) throws URISyntaxException {

        return new ResponseEntity<>(RestResponseUtils
                .create(orderService.create(dto),"ORDER FOOD SUCCESS",200), HttpStatus.OK);
    }

    @GetMapping("/order:fetchList")
    public ResponseEntity<?> fetchList() throws URISyntaxException {
        return new ResponseEntity<>(RestResponseUtils
                .create(orderService.fetchAll(),"ORDER FOOD SUCCESS",200), HttpStatus.OK);
    }

    @GetMapping("/order:fetchByUserId/{userId}")
    public ResponseEntity<?> fetchByUserId(@PathVariable String userId) throws URISyntaxException {
        return new ResponseEntity<>(RestResponseUtils
        .create(orderService.fetchByUserId(userId),"FETCH ORDER BY USER SUCCESS",200), HttpStatus.OK);
    }

    @GetMapping("/order:fetchDetail/{_id}")
    public ResponseEntity<?> fetchDetail(@PathVariable String _id) throws URISyntaxException {
        return new ResponseEntity<>(RestResponseUtils
        .create(orderService.fetchDetail(_id),"FETCH DETAIL SUCCESS",200),HttpStatus.OK);
    }
}
