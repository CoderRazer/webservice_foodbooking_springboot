package com.booking.dev.webrest.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodDetailDTO {

    private String _id;
    private String categoryId;
    private String name;
    private Double price;
    private String description;
    private String pathImage;
    private Boolean available;
}