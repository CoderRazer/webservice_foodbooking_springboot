package com.booking.dev.webrest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCreateDTO {

    private String userId;
    private List<ItemFood> itemFoods;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ItemFood{
        private String idFood;
        private int quantity;
    }


}
