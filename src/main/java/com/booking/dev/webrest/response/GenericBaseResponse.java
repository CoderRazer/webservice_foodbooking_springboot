package com.booking.dev.webrest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericBaseResponse <T> {

    T data;
    Meta meta;

    @Data
    public static class Meta {
        private String message;
        private String errorDetail;
        private Integer statusCode;
    }
}
