package com.booking.dev.webrest.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VueResponse {

    private String accessToken;
    private String _id;
    private String email;
    private Boolean isAdmin;

}
