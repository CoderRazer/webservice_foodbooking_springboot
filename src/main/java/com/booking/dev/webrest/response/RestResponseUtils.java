package com.booking.dev.webrest.response;

import io.vavr.control.Try;
import lombok.val;

import java.util.List;

public class RestResponseUtils {

    public static <T> GenericBaseResponse<?> create(Try<T> result,String message,Integer statusCode) {

        GenericBaseResponse.Meta meta = new GenericBaseResponse.Meta();

        if (result.isSuccess()) {
            val successResult = (Try.Success) result;
            meta.setMessage(message);
            meta.setStatusCode(statusCode);
            meta.setErrorDetail("No Error");
            return new GenericBaseResponse<>(successResult.get(),meta);
        } else {
            val failureResult = (Try.Failure) result;
            meta.setMessage(failureResult.getCause().getMessage());
            meta.setStatusCode(500);
            meta.setErrorDetail(failureResult.getCause().getMessage());
            return new GenericBaseResponse<>(new EmptyDataObject(),meta);
        }
    }

    @SuppressWarnings({"rawtypes"})
    public static <T> GenericBaseResponse<?> create(List<T> result,String message,Integer statusCode) {
        GenericBaseResponse.Meta meta = new GenericBaseResponse.Meta();

        meta.setMessage(message);
        meta.setStatusCode(statusCode);
        meta.setErrorDetail("Nothing");

        return new GenericBaseResponse<>(result, meta);
    }

    public static <T> GenericBaseResponse<?> create(io.vavr.collection.List<T> result, String message, Integer statusCode) {
        GenericBaseResponse.Meta meta = new GenericBaseResponse.Meta();

        meta.setMessage(message);
        meta.setStatusCode(statusCode);
        meta.setErrorDetail("Nothing");

        return new GenericBaseResponse<>(result, meta);
    }



}
