package com.booking.dev.util;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UUIDUtil {

    public String generatorUUID() {

        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        System.out.println("[ Random UUID Gegerated : " + randomUUIDString + " ]");
        return randomUUIDString;
    }
}