package com.booking.dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "orders")
@Data
@AllArgsConstructor
public class Order {

    @Id
    private String _id;
    private String userId;
    private String timeOrder;
//    private Double totalCost;

    public Order(String userId, String timeOrder){
        this.userId = userId;
        this.timeOrder = timeOrder;
    }

}
