package com.booking.dev.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
@Data
@AllArgsConstructor
public class User {

    @Id
    private String _id;
    private String name;

    @Indexed(unique = true)
    private String email;

    @JsonIgnore
    private String password;
    private String phone;
    private String address;
    private Boolean isAdmin;

    public User(String name, String email, String password, String phone, String address, Boolean isAdmin){
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
        this.isAdmin = isAdmin;
    }
}
