package com.booking.dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "categorys")
@Data
@AllArgsConstructor
public class Category {

    @Id
    private String _id;

    @Indexed(unique = true)
    private String name;
    private String pathImage;

    public Category(String name, String pathImage){
        this.name = name;
        this.pathImage = pathImage;
    }

}
