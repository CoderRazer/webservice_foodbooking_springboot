package com.booking.dev.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "item")
@Data
@AllArgsConstructor
public class Item {

    @Id
    private String _id;
    private String orderId;
    private Food food;
    private int quantity;

    public Item(String orderId, Food food, int quantity){
        this.orderId = orderId;
        this.food = food;
        this.quantity = quantity;
    }

}
