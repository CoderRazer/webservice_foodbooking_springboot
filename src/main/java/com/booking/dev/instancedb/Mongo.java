package com.booking.dev.instancedb;

import com.mongodb.MongoClientURI;
import com.mongodb.MongoClient;

public class Mongo {

    private static Mongo mongoInstance = null;
    private static MongoClient mongoClient = null;

    public static Mongo getInstance(){
        if (mongoInstance == null){
            synchronized (Mongo.class){
                if(mongoInstance == null){
                    mongoInstance = new Mongo();
                }
            }
        }
        return mongoInstance;
    }

    public MongoClient getConnectionMongoDB(){
        MongoClientURI mongoClientURI = new MongoClientURI("mongodb://localhost:27017");
        mongoClient = new MongoClient(mongoClientURI);
        return mongoClient;
    }
}
