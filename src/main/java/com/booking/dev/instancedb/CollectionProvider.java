package com.booking.dev.instancedb;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;


public class CollectionProvider {

    private static CollectionProvider providerInstance = null;

    private final MongoClient mongoClient = Mongo.getInstance().getConnectionMongoDB();
    private MongoDatabase database = mongoClient.getDatabase("food_booking");

    public static CollectionProvider getInstance(){
        if (providerInstance == null){
            synchronized (Mongo.class){
                if(providerInstance == null){
                    providerInstance = new CollectionProvider();
                }
            }
        }
        return providerInstance;
    }

    public MongoCollection<Document> getCollection(String collection){
        return database.getCollection(collection);
    }

}
