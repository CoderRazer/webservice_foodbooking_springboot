package com.booking.dev.repository;

import com.booking.dev.domain.Item;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends MongoRepository<Item,String> {

    List<Item> findAllByOrderId(String orderId);

}