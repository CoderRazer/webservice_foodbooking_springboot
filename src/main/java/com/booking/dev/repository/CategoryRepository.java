package com.booking.dev.repository;

import com.booking.dev.domain.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends MongoRepository<Category,String> {

    List<Category> findAllByNameContainsOrderByName(String name);

}