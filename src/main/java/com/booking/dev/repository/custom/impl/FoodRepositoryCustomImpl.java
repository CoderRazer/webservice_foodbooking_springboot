package com.booking.dev.repository.custom.impl;

import com.booking.dev.instancedb.CollectionProvider;
import com.booking.dev.repository.custom.FoodRepositoryCustom;
import com.booking.dev.webrest.dto.FoodDetailDTO;
import com.booking.dev.webrest.dto.FoodUpdateDTO;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import io.vavr.control.Try;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

import java.util.ArrayList;
import java.util.List;

@Service
public class FoodRepositoryCustomImpl implements FoodRepositoryCustom {

    private final Logger logger = LoggerFactory.getLogger(FoodRepositoryCustomImpl.class);

    private final MongoCollection<Document> collection = CollectionProvider.getInstance().getCollection("foods");

    @Override
    public List<FoodDetailDTO> getByCategory(String categoryId) {

        Bson filter = Filters.eq("categoryId", categoryId);
        Bson keySort = ascending("name");
        FindIterable<Document> list = collection.find(filter).sort(keySort);

        List<FoodDetailDTO> result = new ArrayList<>();

        for (Document doc : list) {
            result.add(
                    new FoodDetailDTO(
                            doc.getObjectId("_id").toString(),
                            doc.getString("categoryId"),
                            doc.getString("name"),
                            doc.getDouble("price"),
                            doc.getString("description"),
                            doc.getString("pathImage"),
                            doc.getBoolean("available")
                    )
            );
        }
        return result;
    }

    @Override
    public Try<Boolean> update(String _id, FoodUpdateDTO dto) {

        return Try.of(() -> {

            Bson filter = Filters.eq("_id",new ObjectId(_id));
            Bson query = combine(
                    set("categoryId",dto.getCategoryId()),
                    set("name",dto.getName()),
                    set("price",dto.getPrice()),
                    set("description",dto.getDescription()),
                    set("pathImage",dto.getPathImage()),
                    set("available",dto.getAvailable())
            );
            collection.updateOne(filter,query);
            return true;
        });

    }
}
