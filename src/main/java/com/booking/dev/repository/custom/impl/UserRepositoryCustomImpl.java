package com.booking.dev.repository.custom.impl;

import com.booking.dev.instancedb.CollectionProvider;
import com.booking.dev.repository.custom.UserRepositoryCustom;
import com.booking.dev.webrest.dto.UserUpdateDTO;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import io.vavr.control.Try;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import static com.mongodb.client.model.Updates.*;

@Service
public class UserRepositoryCustomImpl implements UserRepositoryCustom {

    private final MongoCollection<Document> collection = CollectionProvider.getInstance().getCollection("users");

    private final Logger logger = LoggerFactory.getLogger(UserRepositoryCustomImpl.class);

    @Override
    public Try<Boolean> update(String _id, UserUpdateDTO dto) {
        return Try.of(() -> {
            Bson filter = Filters.eq("_id",new ObjectId(_id));
            Bson query = combine(
                    set("name",dto.getName()),
                    set("phone",dto.getPhone()),
                    set("address",dto.getAddress()));
                    collection.updateOne(filter,query);
                    return true;
        });
    }
}
