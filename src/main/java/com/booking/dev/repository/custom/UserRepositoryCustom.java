package com.booking.dev.repository.custom;

import com.booking.dev.webrest.dto.UserUpdateDTO;
import io.vavr.control.Try;

public interface UserRepositoryCustom {

    Try<Boolean> update(String _id, UserUpdateDTO dto);
}
