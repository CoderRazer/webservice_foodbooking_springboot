package com.booking.dev.repository.custom;

import com.booking.dev.webrest.dto.FoodDetailDTO;
import com.booking.dev.webrest.dto.FoodUpdateDTO;
import io.vavr.control.Try;

import java.util.List;


public interface FoodRepositoryCustom {

    List<FoodDetailDTO> getByCategory(String categoryId);

    Try<Boolean> update(String _id, FoodUpdateDTO dto);

}