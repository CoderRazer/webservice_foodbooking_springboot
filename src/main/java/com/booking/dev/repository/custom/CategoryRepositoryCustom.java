package com.booking.dev.repository.custom;

import com.booking.dev.webrest.dto.CategoryCreateDTO;
import io.vavr.control.Try;

public interface CategoryRepositoryCustom {

    Try<Boolean> create(CategoryCreateDTO dto);

    Try<Boolean> update(String _id, String name);

}
