package com.booking.dev.repository.custom.impl;

import com.booking.dev.instancedb.CollectionProvider;
import com.booking.dev.repository.custom.CategoryRepositoryCustom;
import com.booking.dev.webrest.dto.CategoryCreateDTO;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import io.vavr.control.Try;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

@Service
public class CategoryRepositoryCustomImpl implements CategoryRepositoryCustom {

    private final MongoCollection<Document> collection = CollectionProvider.getInstance().getCollection("categorys");

    private final Logger logger = LoggerFactory.getLogger(CategoryRepositoryCustomImpl.class);

    @Override
    public Try<Boolean> create(CategoryCreateDTO dto) {
        return Try.of(() -> {
            collection.insertOne(
                    new Document("name", dto.getName()));
            return true;
        });
    }

    @Override
    public Try<Boolean> update(String _id, String name) {
        Bson filter = Filters.eq("_id", new ObjectId(_id));
        Bson query = combine(set("name", name));
        return Try.of(() -> {
            collection.updateOne(filter, query);
            return true;
        });
    }

}
